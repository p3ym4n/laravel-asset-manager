<?php
/**
 * Created by PhpStorm.
 * User: p3ym4n
 * Date: 2/19/2016 AD
 * Time: 15:42
 */

return [

	/**
	 * The Bower Main Path
	 */

	'bower_dir' => base_path('/bower_components'),

	/**
	 * Path in public folder that cached files goes there.
	 */

	'public_cache_folder' => 'cache',
];