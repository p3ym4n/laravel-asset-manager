<?php

namespace p3ym4n\AssetManager;

use Illuminate\Support\ServiceProvider;
use p3ym4n\AssetManager\Console\AssetClearCommand;
use p3ym4n\AssetManager\Console\AssetMakeCommand;

class AssetServiceProvider extends ServiceProvider {
    
    /**
     * service load is different
     * @var bool
     */
    protected $defer = true;
    
    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot() {
        //registering the console command
        if ($this->app->runningInConsole()) {
            $this->commands([
                AssetMakeCommand::class,
                AssetClearCommand::class,
            ]);
        }
        
        //adding configs
        $this->publishes([
            __DIR__ . '/Config/asset.php' => config_path('asset.php'),
        ], 'config');
    }
    
    /**
     * Register the application services.
     * @return void
     */
    public function register() {
        $this->app->singleton(Asset::class, function () {
            return new Asset;
        });
    }
    
    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides() {
        return [Asset::class];
    }
    
}
