<?php
/**
 * Created by PhpStorm.
 * User: p3ym4n
 * Date: 1/13/2016 AD
 * Time: 17:58
 */

namespace p3ym4n\AssetManager;

use Facades\p3ym4n\AssetManager\Asset;

class BaseAsset {
    
    //these properties is for children
    protected $baseDir = '';
    protected $css = [];
    protected $js = [];
    protected $style = '';
    protected $script = '';
    protected $include = [];
    protected $ignore = [];
    protected $cssOption = [];
    protected $jsOption = [];
    
    protected $chmod = 0777;
    protected $forceCopy = false;
    protected $forceDelete = false;
    protected $cacheFolder = null;
    
    //these properties is for parent class
    private $name;
    private $loop = [];
    private $destination = [];
    private $copy = [];
    private $target = '';
    private static $instance;
    
    /**
     * making the app object
     * @return static
     */
    private static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }
    
    /**
     * calls from the subclass and starts the process
     */
    public static function add() {
        self::getInstance()->run('add');
    }
    
    /**
     * calls from the subclass and starts the process
     */
    public static function del() {
        self::getInstance()->run('del');
    }
    
    /**
     * run all the process and give the result to AssetHolder
     *
     * @param $type
     */
    private function run($type) {
        //getting the defined array of assets
        //this method will define in the children classes
        $this->init();
        
        //making the base path of public dir
        $this->getTarget();
        
        //duplication check
        if ($type == 'del') {
            return Asset::delAsset($this->name);
        }
        
        //find all of them between patterns and folders
        $this->boot();
        
        //check for those that have to copy to their cache folders
        foreach ($this->loop as $kind => $contents) {
            if ( ! empty($contents)) {
                foreach ($contents as $index => $content) {
                    if ( ! $this->isPublic($this->baseDir . $content)) {
                        $index                     = $this->baseDir . $content;
                        $this->copy[$kind][$index] = public_path() . $this->target . $content;
                    } else {
                        $this->target = '';
                    }
                    $this->destination[$kind][] = $this->target . $content;
                }
            }
        }
        
        //remove the ignored items from all other kinds
        if ( ! empty($this->loop['ignore'])) {
            foreach ($this->loop as $kind => $contents) {
                if ($kind != 'ignore' && ! empty($contents)) {
                    $this->copy[$kind]        = array_diff($this->copy[$kind], $this->copy['ignore']);
                    $this->destination[$kind] = array_diff($this->destination[$kind], $this->destination['ignore']);
                }
            }
            unset($this->copy['ignore']);
            unset($this->destination['ignore']);
        }
        
        //un setting the include index from destination
        if (array_key_exists('include', $this->destination)) {
            unset($this->destination['include']);
        }
        
        //flatting the copy array
        $copyTemp = [];
        foreach ($this->copy as $kind => $contents) {
            foreach ($contents as $from => $to) {
                $copyTemp[$from] = $to;
            }
        }
        $this->copy = $copyTemp;
        
        //detect type of command and then we pass the data to the AssetHolder
        if ($type == 'add') {
            $result = array_merge($this->destination, [
                'name'      => $this->name,
                'copy'      => $this->copy,
                'forceCopy' => $this->forceCopy,
                'chmod'     => $this->chmod,
                'style'     => $this->style,
                'script'    => $this->script,
                'cssOption' => $this->cssOption,
                'jsOption'  => $this->jsOption,
            ]);
            Asset::addAsset($result);
        }
        
        //we must destroy the previous object!
        self::$instance = null;
    }
    
    /**
     * load all the data from the subclass init() method
     * and loops over them for finding all files
     */
    private function boot() {
        /**
         * making this loop for all type of assets
         * that we include and user in out sub classes
         */
        $this->loop = [
            'include' => $this->include,
            'ignore'  => $this->ignore,
            'css'     => $this->css,
            'js'      => $this->js,
        ];
        
        //modifying the baseDir for use
        $temp_base_dir = $this->baseDir;
        if ( ! is_dir($temp_base_dir)) {
            $temp_base_dir = config('asset.bower_dir') . $temp_base_dir;
        }
        $temp_base_dir = rtrim($temp_base_dir, '/') . '/';
        
        /**
         * foreach contents we search glob patterns or arrays
         * and list of the file that should be included
         */
        foreach ($this->loop as $kind => $contents) {
            if ( ! empty($contents)) {
                if ( ! is_array($contents)) {
                    $contents = [$contents];
                }
                $contentTemp = [];
                foreach ($contents as $content) {
                    if (preg_match("/\*/", $content)) {
                        $temp = $this->glob($temp_base_dir . $content);
                        foreach ($temp as $k => $contentFile) {
                            $contentTemp[] = str_replace($temp_base_dir, '', $contentFile);
                        }
                    } else {
                        $contentTemp[] = $content;
                    }
                }
                $this->loop[$kind] = $contentTemp;
            }
        }
    }
    
    /**
     * recursive traverse on a folder to find all files
     *
     * @param $pattern
     *
     * @return array
     */
    private function glob($pattern) {
        $files = glob($pattern);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->glob($dir . '/' . basename($pattern)));
        }
        return $files;
    }
    
    /**
     * check if the given directory is in the app public dir or not
     *
     * @param $dir
     *
     * @return bool
     */
    private function isPublic($dir) {
        if (strpos($dir, public_path()) !== false) {
            return true;
        }
        return false;
    }
    
    /**
     * return the folder path of the assets in cache folder
     * @return string
     */
    private function getTarget() {
        $this->name = str_replace('Asset', '', class_basename(static::class));
        $name       = $this->name;
        if ( ! empty($this->cacheFolder)) {
            $name = $this->cacheFolder;
        }
        
        $cacheName = trim(config('asset.public_cache_folder'), '/');
        
        $this->target = '/' . $cacheName . '/' . studly_case($name) . '/';
    }
    
    /**
     * this method is for child classes to get the relative target path
     * @return string
     */
    protected function target() {
        $this->getTarget();
        return $this->target;
    }
    
}