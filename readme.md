# Laravel 5 & Bower Asset Manager
## Install

Require this package with composer using the following command:

```bash
composer require p3ym4n/laravel-asset-manager
```

After updating composer, add the service provider to the `providers` array in `config/app.php`

```php
p3ym4n\AssetManager\AssetServiceProvider::class,
```

And add the Facade to the `aliases` array in `config/app.php`

```php
'Asset' => p3ym4n\AssetManager\Facades\Asset::class,
```

Then initialize the bower ( and make your `bower.json` file if you want) , by default the assets will copy to `bower_components` folder in the root folder of project.

You can also publish the config file to change the defaults ( bower_dir & public_cache_folder) .

```bash
php artisan vendor:publish --provider="p3ym4n\AssetManager\AssetServiceProvider" --tag=config
```

## Console Commands
 
After adding the package you will have 2 new commands :

### 1. Making new asset files under `App\Http\Assets` namespace 

```bash
php artisan asset:make bootstrap
```

Here the `bootstrap` will become the name of the package and the `app/Http/Assets/BootstrapAsset.php` file will generate.

In the generated class there's only one method named `init` and in that you should define the base path of the package folder relative from `bower_dir` and your assets Or an absolute path to a folder in the public dir.
See this example :

```php
final class BootstrapAsset extends BaseAsset

    protected function init()
    {
        $this->baseDir = '/bootstrap-rtl/dist/';

        $this->css = [          // you can use * for all files
            'css/*.min.css'     
        ];

        $this->js = [
            'js/bootstrap.min.js'
        ];

        $this->include = [      // also you can include some other files that needed in the package
            'fonts/*'           
        ];

                                
        $this->script =         // you can write your own custom initializations or other stuff... 
        "       
            $(function(){
                $('[data-toggle=\"tooltip\"]').tooltip({
                    container : 'body'
                });

                $('[data-toggle=\"popover\"]').popover({
                    container : 'body'
                });
            });
        ";
        
        $this->style =          // also you can add your custom styles & css 
        "        
            body{
                font-size: 14px;
            }
        ";
    }
}
```

There are five properties that you can initialize :  `css`, `js`, `include`, `script`, `style` .
As you see in example the `css`, `js` ,`include` values are paths to files from `$this->baseDir` . 
Otherwise `script` and `style` consist inline javascript and css .

### 2. Clearing the public_cache_folder

Sometimes you may want to clear the cache folder.

```bash
php artisan asset:clear
```

## How to use packages

Packages can be added in your controllers or providers ( boot methods ) by calling :

```php
BootstrapAsset::add();
```

Or removing the package by calling :
 
```php
BootstrapAsset::del();
```
## Asset Holder

In this package we also have an Asset Holder (the `Asset` facade) , that have these methods :
 
```php
Asset::addCss("path/to/css/file");
Asset::delCss("path/to/css/file");

Asset::addJs("path/to/js/file");
Asset::delJs("path/to/js/file");

Asset::addStyle(" some css ... ");
Asset::delStyle(" some css ... ");

Asset::addScript(" some js ... ");
Asset::delScript(" some js ... ");

```

### The Final List of assets for attaching in views

After all adding and manipulating your can get the assets as an `array` with these commands ,
All defined files will automatically copied to the cache folder and url to files is accessible through below calls.

```php
Asset::get('css');          //returns an array       

Asset::get('js');           //returns an array

Asset::get('style');        //returns a string
    
Asset::get('script');       //returns a string       
```

## An example to use

##### here I show you my way of using this package.

In every project we have some default packages that we use them in all pages of the project.
so its good to add them in `AppServiceProvider.php` boot method. see mine :

```php
public function boot()
{

    JqueryAsset::add();
    AjaxFormAsset::add();
    CookieAsset::add();
    BootstrapAsset::add();
    FontAwesomeAsset::add();
    SortableAsset::add();
    SpinAsset::add();   
    AppAsset::add();

    view()->composer('partials.css', function ($view) {
        $view->with([
            'allCss' => Asset::get('css'),
            'styles' => Asset::get('style'),
        ]);
    });

    view()->composer('partials.js', function ($view) {
        $view->with([
            'allJs'   => Asset::get('js'),
            'scripts' => Asset::get('script'),
        ]);
    });

}

```

And I have two blade templates for including my css & js files . As you can see in the recent code block when they'll render
we will send the result as `allCss`,`styles`,`allJs`,`scripts` variables.
now lets take a look at templates :
 
```php
// views/partials/css.blade.php

@if(isset($allCss) && !empty($allCss))
    <!-- Styles -->
    @foreach($allCss as $item)
        <link href="{{$item}}" rel="stylesheet">
    @endforeach
@endif

@if(isset($styles) && !empty($styles))
    <style>
        {!! $styles !!}
        @yield('style')
    </style>
@endif



// views/partials/js.blade.php
    
@if(isset($allJs) && !empty($allJs))
    <!-- JavaScripts -->
    @foreach($allJs as $item)
        <script src="{{$item}}"></script>
    @endforeach
@endif

@if(isset($scripts) && !empty($scripts))
    <script type="text/javascript">
        {!! $scripts !!}
        @yield('script')
    </script>
@endif
```
 So you can include your `css.blade.php` or `js.blade.php` template every where that you want.