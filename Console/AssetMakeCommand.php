<?php

namespace p3ym4n\AssetManager\Console;

use Illuminate\Console\Command;

class AssetMakeCommand extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'asset:make {name : name of the asset manager class }';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a new Asset Manager class';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//getting the name from the CLI
		$name = $this->argument('name');

		//making the Class name
		$className = studly_case($name) . 'Asset';

		//generating the class contents
		$content = '<?php

namespace App\Http\Assets;

use p3ym4n\AssetManager\BaseAsset;

/**
 * Class ' . $className . '
 * @package App\Http\Assets
 */
final class ' . $className . ' extends BaseAsset
{
    protected function init()
    {
        $this->baseDir = \'/' . str_slug($name) . '/\';

        $this->css = [

        ];

        $this->style = "

        ";

        $this->js = [

        ];

        $this->script = "

        ";

        $this->include = [

        ];

    }
}';

		$filePath = app_path('Http/Assets/' . $className . '.php');
		if (file_exists($filePath)) {
			if (!$this->confirm('\'' . $className . '.php\' already exists, do you want to overwrite? [y|n]')) {
				return;
			}
		}

		if (!is_dir($filePath)) {
			@mkdir(pathinfo($filePath, PATHINFO_DIRNAME));
		}

		$fp = fopen($filePath, "wb");
		fwrite($fp, $content);
		fclose($fp);

		return $this->info('"' . $className . '" Class has been created.');

	}
}
