<?php

namespace p3ym4n\AssetManager\Console;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class AssetClearCommand extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'asset:clear';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'clears the assets cache folder';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$path = public_path(config('asset.public_cache_folder'));

		$fileSystem = new Filesystem();
		$fileSystem->cleanDirectory($path);

		return $this->info("'$path' folder cleared. ");
	}
}
