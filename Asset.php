<?php
/**
 * Created by PhpStorm.
 * User: p3ym4n
 * Date: 1/14/2016 AD
 * Time: 02:06
 */

namespace p3ym4n\AssetManager;

final class Asset {
    
    private $run = false;
    private $holder = [];
    private $style = '';
    private $css = [];
    private $script = '';
    private $js = [];
    private $obsolete = [];
    
    /**
     * adds the asset to the main holder
     *
     * @param array $Asset
     */
    public function addAsset(array $Asset) {
        if (isset($Asset['name']) && ! array_key_exists($Asset['name'], $this->holder)) {
            $this->holder[$Asset['name']] = $Asset;
        }
    }
    
    /**
     * delete the asset from the main holder
     *
     * @param array $Asset
     */
    public function delAsset(array $Asset) {
        if (isset($Asset['name']) && array_key_exists($Asset['name'], $this->holder)) {
            unset($this->holder[$Asset['name']]);
        }
    }
    
    /**
     * adds css to the css list
     *
     * @param $link
     */
    public function addCss($link) {
        if ( ! empty($link)) {
            if ( ! is_array($link)) {
                $link = [$link];
            }
            $this->css = array_merge($this->css, $link);
        }
    }
    
    /**
     * adds js to the js list
     *
     * @param $link
     */
    public function addJs($link) {
        
        if ( ! empty($link)) {
            if ( ! is_array($link)) {
                $link = [$link];
            }
            $this->js = array_merge($this->js, $link);
        }
    }
    
    /**
     * deleted css from the css list
     *
     * @param $link
     */
    public function delCss($link) {
        if ( ! empty($link)) {
            if ( ! is_array($link)) {
                $link = [$link];
            }
            if ( ! isset($this->obsolete['css'])) {
                $this->obsolete['css'] = [];
            }
            $this->obsolete['css'] = array_merge($this->obsolete['css'], $link);
        }
    }
    
    /**
     * deletes js from the js list
     *
     * @param $link
     */
    public function delJs($link) {
        if ( ! empty($link)) {
            if ( ! is_array($link)) {
                $link = [$link];
            }
            if ( ! isset($this->obsolete['js'])) {
                $this->obsolete['js'] = [];
            }
            $this->obsolete['js'] = array_merge($this->obsolete['js'], $link);
        }
    }
    
    /**
     * adds the $style to styles
     *
     * @param $style
     */
    public function addStyle($style) {
        $style = trim($style);
        if ($style != '') {
            if (strpos($this->style, $style) === false) {
                $this->style .= " 
                $style ";
            }
        }
    }
    
    /**
     * adds the $script to scripts
     *
     * @param $script
     */
    public function addScript($script) {
        $script = trim($script);
        if ($script != '') {
            if (strpos($this->script, $script) === false) {
                $this->script .= " 
				$script";
            }
        }
    }
    
    /**
     * this method add the init script into the first of scripts
     *
     * @param $script
     */
    private function addScriptBefore($script) {
        $script = trim($script);
        if ($script != '') {
            if (strpos($this->script, $script) === false) {
                $this->script = "
				 {$script} 
				 {$this->script}";
            }
        }
    }
    
    /**
     * deletes the given $style from the styles property
     *
     * @param $style
     */
    public function delStyle($style) {
        if ( ! empty($style)) {
            if ( ! is_array($style)) {
                $style = [$style];
            }
            if ( ! isset($this->obsolete['style'])) {
                $this->obsolete['style'] = [];
            }
            $this->obsolete['style'] = array_merge($this->obsolete['style'], $style);
        }
    }
    
    /**
     * delete the given $script from the scripts property
     *
     * @param $script
     */
    public function delScript($script) {
        if ( ! empty($script)) {
            if ( ! is_array($script)) {
                $script = [$script];
            }
            if ( ! isset($this->obsolete['script'])) {
                $this->obsolete['script'] = [];
            }
            $this->obsolete['script'] = array_merge($this->obsolete['script'], $script);
        }
    }
    
    /**
     * removes all the added style , scripts & assets
     */
    public function reset() {
        $this->script   = '';
        $this->style    = '';
        $this->js       = [];
        $this->css      = [];
        $this->obsolete = [];
        $this->holder   = [];
        $this->run      = false;
    }
    
    /**
     * copy all files and folders from $src to $dst
     *
     * @param      $src
     * @param      $dst
     * @param bool $force
     * @param int  $chmod
     */
    private function copy($src, $dst, $force = false, $chmod = 0777) {
        $path = pathinfo($dst);
        if ( ! is_dir($path['dirname']) || $force) {
            @mkdir($path['dirname'], $chmod, true);
        }
        if ( ! file_exists($dst) || $force) {
            @copy($src, $dst);
        }
    }
    
    /**
     * a proxy to __get method for use with facade
     *
     * @param $name
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($name) {
        $this->runCopy();
        if (in_array($name, ['css', 'js', 'style', 'script'])) {
            $return = $this->{$name};
        } else {
            throw new \Exception(" '$name' doesn't Exists is the '" . __CLASS__ . "' class. ");
        }
        return $return;
    }
    
    /**
     * return the wanted asset from this list : css , js , style , script
     *
     * @param $name
     *
     * @return mixed
     * @throws \Exception
     */
    public function __get($name) {
        return $this->get($name);
    }
    
    /**
     * does all the copying and making the last cleaned arrays for use
     * this method run only once per request
     */
    private function runCopy() {
        if ( ! $this->run) {
            foreach ($this->holder as $name => $contents) {
                //checking the copy index
                if (array_key_exists('copy', $contents)) {
                    
                    //forceCopy flag check
                    if ( ! array_key_exists('forceCopy', $contents)) {
                        $contents['forceCopy'] = false;
                    }
                    
                    //chmod flag check
                    if ( ! array_key_exists('chmod', $contents)) {
                        $contents['chmod'] = 0777;
                    }
                    
                    //iterating
                    foreach ($contents['copy'] as $from => $to) {
                        $this->copy($from, $to, $contents['forceCopy'], $contents['chmod']);
                    }
                }
                
                //some scripts have to initialized before they call statements
                if (array_key_exists('script', $contents)) {
                    $this->addScriptBefore($contents['script']);
                }
                
                if (array_key_exists('style', $contents)) {
                    $this->addStyle($contents['style']);
                }
                
                if (array_key_exists('css', $contents)) {
                    $this->addCss($contents['css']);
                }
                
                if (array_key_exists('js', $contents)) {
                    $this->addJs($contents['js']);
                }
            }
            
            //all css
            if (array_key_exists('css', $this->obsolete)) {
                $this->css = array_diff($this->css, $this->obsolete['css']);
            }
            $this->css = array_unique($this->css);
            
            //all js
            if (array_key_exists('js', $this->obsolete)) {
                $this->js = array_diff($this->js, $this->obsolete['js']);
            }
            $this->js = array_unique($this->js);
            
            //all scripts
            if (array_key_exists('script', $this->obsolete)) {
                $this->script = str_replace($this->obsolete['script'], '', $this->script);
            }
            
            //all styles
            if (array_key_exists('style', $this->obsolete)) {
                $this->style = str_replace($this->obsolete['style'], '', $this->style);
            }
            
            //the copy has been executed!
            $this->run = true;
        }
    }
}